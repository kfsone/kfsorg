require 'rubygems'
require 'sinatra'
require 'liquid'
require 'sequel'

set :environment, ENV['RACK_ENV'].to_sym
disable :run, :reload

require './kfs.org.rb'

run Sinatra::Application

