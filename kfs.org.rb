require 'sinatra'
require 'liquid'

require './tags.liquid.rb'

configure do

	mime_type :jpg,  'image/jpeg'
	mime_type :jpeg, 'image/jpeg'
	mime_type :gif,  'image/gif'
	mime_type :png,  'image/png'
	mime_type :bmp,  'image/bmp'

	mime_type :mp3,  'audio/mpeg'

	mime_type :css,  'text/css'
	mime_type :txt,  'text/plain'
	mime_type :js,   'application/javascript'
	mime_type :json, 'application/json'

	mime_type :exe,  'application/x-msdownload'
	mime_type :gz,   'application/octet-stream'
	mime_type :tgz,  'application/octet-stream'

end

get %r{^(.*)/\s*$} do |path|
	call env.merge("PATH_INFO" => "#{path}/index.html")
end

get %r{\.liquid$} do
	"<h3>Access Denied</h3>"
end

get %r{^/(.*)\.html} do |template|
	liquid :"#{template}"
end
