#! /bin/bash

#################### Configuration
#

#### Misc

defaultPath="/var/www/kfs.org"
nginxDir="/etc/nginx"
nginxConfigFile="${nginxDir}/nginx.conf"

#### Source files

# File with the passenger ppa
passengerPpa="etc/passenger.list"

# Nginx site config file
nginxSiteConf="etc/kfs.org.nginx-site"

#### Destinations

passengerPpaDest="/etc/apt/sources.list.d/passenger.list"
nginxSiteConfDest="${nginxDir}/sites-available/kfs.org"


#################### Helper functions
#

if [ -f "etc/helpers.lib.sh" ]; then
  . etc/helpers.lib.sh
else
  if [ -f "${defaultPath}/etc/helpers.lib.sh" ]; then
    . "${defaultPath}/etc/helpers.lib.sh"
  else
    echo "ERROR: Unable to locate helpers.lib.sh"
    exit 1
  fi
fi

# Return 0 or 1 if a given directory looks like our base
function is_base_dir () {
	debug "testing $1"
	test_for -d "$1";					if [ $? -eq 0 ]; then return 0; fi
	test_for -f "$1/config.ru"; 		if [ $? -eq 0 ]; then return 0; fi
	test_for -d "$1/etc"; 				if [ $? -eq 0 ]; then return 0; fi
	test_for -f "$1/${nginxSiteConf}";	if [ $? -eq 0 ]; then return 0; fi
	test_for -f "$1/${passengerPpa}";	if [ $? -eq 0 ]; then return 0; fi
	return 1
}


#################### Preparation
#

# Where we're running
curPath=`pwd`

echo "User = ${USER}"
echo "Path = ${curPath}"

if [ \( "${USER}" != "root" -a "${USER}" != "vagrant" \) -o \
     \( "${curPath}" != "/home/vagrant" -a "${curPath}" != "${defaultPath}" \) ]; then
  die "This script is intended to be run from inside a Vagrant machine"
fi

# Determine if we're in the default path
isDefault=1
is_base_dir "."
if [ $? -eq 0 ]; then
  # We aren't, but the default path might be in-place
  # (this is primarily to support vagrant :)
  debug "Not in base directory, checking ${defaultPath}"
  is_base_dir "${defaultPath}"
  if [ $? -eq 0 ]; then
    die "Not running from in base directory, or one or more files/directories is missing."
  fi
  cd "${defaultPath}" || die "Not running from base directory and unable to access ${defaultPath}"
  curPath="${defaultPath}"
else
  echo "else"
  if [ "${curPath}" != "${defaultPath}" ]; then
    isDefault=0;
  fi
fi

# If we're not in the default path, require user action.
if [ -z "${FORCE}" -a $isDefault -eq 0 ]; then
  echo "This script expects to be run from ${defaultPath} (ran from ${curPath} instead)."
  echo "To run it from somewhere else, please use FORCE=1 $0"
  exit 1
fi


#################### BEGIN INSTALLATION
#

#### Ensure system is up-to date

# Install passenger ppa
if [ ! -f "${passengerPpaDest}" ]; then
  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 561F9B9CAC40B2F7 || die "apt-key adv failed"
  sudo cp "${passengerPpa}" "${passengerPpaDest}" || die "Unable to copy ${passengerPpa} to ${passengerPpaDest}"
  sudo chown root: "${passengerPpaDest}" || die "Unable to chown ${passengerPpaDest}"
  sudo chmod 644 "${passengerPpaDest}" || die "Unable to chmod ${passengerPpaDest}"
fi

# Upgrade packages
sudo apt-get update || die "apt-get update for passenger failed"
sudo apt-get -y upgrade || die "apt-get upgrade failed"

sudo apt-get -y install \
	ruby2.0 ruby2.0-dev ruby2.0-doc	\
	vim git dos2unix \
	apt-transport-https ca-certificates \
		|| die "apt-get main installation failed"


#### Install nginx/passenger
#

# install nginx web server
sudo apt-get install -y nginx nginx-common nginx-extras nginx-doc || die "Failed installing nginx"

# install passenger itself
sudo apt-get install -y passenger || die "Failed installing passenger"


#### System configuration
#

# Enable passenger in nginx
sudo perl -i -pe 's/^(\s*)\#\s*(passenger_)/$1$2/' "${nginxConfigFile}" || die "Failed to enable passenger"
sudo cp "${nginxSiteConf}" "${nginxSiteConfDest}" || die "Unable top copy ${nginxSiteConf} to ${nginxSiteConfDest}"

# Add user ubuntu to www-data group
sudo perl -i -pe 's/^(www-data:.*):$/$1:ubuntu/' /etc/group


#### Directory structure/ownership
#

if [ ! -d './tmp' ]; then mkdir tmp; fi

if [ $isDefault -eq 1 ]; then
  sudo chown -R www-data:www-data /var/www
  sudo chmod -R u+rwx /var/www
  sudo chmod -R g+rwxs /var/www
else
  echo "WARNING: Non-default install, check directory ownership. Should be group www-data."
fi

#### Install ruby gems

sudo gem install sinatra liquid sequel


#### Site configuration
#

sudo perl -i -pe "s^\#{PATH}\#^${curPath}^g" "${nginxSiteConfDest}" || die "Unable to replace #{PATH}# with ${curPath} in ${nginxSiteConfDest}"
sudo chown root: "${nginxSiteConfDest}" || die "Unable to chown ${nginxSiteConfDest}"
sudo rm -f "${nginxDir}"/sites-enabled/{default,kfs.org}
sudo ln -s "${nginxSiteConfDest}" "${nginxDir}/sites-enabled/" || die "Unable to symlink kfs.org to sites-enabled"


#### Cleanup
#

# Restart web service
sudo service nginx restart

