KFSORG Vagrant+Sinatra+Liquid+Sequel bootstrapper
=================================================

Provides quick setup of a local virtual machine for experimenting with the Sinatra framework with Liquid and Sequel.


REQUIREMENTS
============

1. Virtual Machine provider

	Either VirtualBox or VMWare; for others, consult the vagrant documentation.
	https://www.virtualbox.org/

2. Vagrant

	Automates the virtual machine deployment and provisioning.
	http://www.vagrantup.com/

3. Bash

	If you're running Windows, install git bash
	http://msysgit.github.io/


INSTALLATION
============

1. Clone the repository to your local machine, e.g.

 user@~$ cd $TMP
 user@/tmp$ git clone git@bitbucket.org:kfsone/kfsorg.git kfs.org
 user@/tmp$ cd kfs.org

2. Using a bash shell run the bootstrap.sh script,
 user@/tmp/kfs.org$ ./bootstrap.sh

3. Access the webserver
 http://localhost:8080/

4. Log in to the Linux environment
 user@/tmp/kfs.org$ vagrant ssht

5. To shutdown the virtual machine

 user@/tmp/kfs.org$ vagrant destroy


ADDITIONAL INFO
===============

- Static HTML is served from "public", the Liquid template files live under "views" (they can be nested),

- To have Sinatra pick up changes to files,

 user@/tmp/kfs.org$ touch tmp/restart.txt

- Files are auto-shared with the virtual machine so you edit them either locally or in the vm,

