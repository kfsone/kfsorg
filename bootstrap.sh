#! /bin/bash

. etc/helpers.lib.sh

#################### Vagrant box setup.
#
# You'll need to have 'vagrant' installed
# This script installs the box image for you, registers it as 'trusty',
# and brings the machine up provisioned.
#
# You'll also need a provider: virtualbox or vmware. Default is virtualbox.

if [ -d ".vagrant" ]; then
  die "WARNING: .vagrant folder already exists. Aborting."
fi

#################### Configuration

# You can override these with, e.g. PROVIDER=vmware ./bootstrap.sh

# Which virtual machine provider to use.
PROVIDER=${PROVIDER:-virtualbox}

# Local name for the box.
BOXNAME=${BOXNAME:-trusty}

# Source of the box.
BOXURL=${BOXURL:-https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box}


# Add the box to the local system
echo "INFO: Downloading/registering ${BOXNAME} ${PROVIDER} box from ${BOXURL}"
echo "INFO: If this has already been registered, you can ignore the error from vagrant"
vagrant box add -c --provider "${PROVIDER}" "${BOXNAME}" "${BOXURL}"

# Fire up the virtual machien and provision it.
echo "Starting the virtual machine and install script"
vagrant up --provision --provider virtualbox --parallel

echo "Ready to go."
