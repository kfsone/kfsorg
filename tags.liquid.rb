# vim: set ts=4:

require 'liquid'

class PageTag < Liquid::Block
	def initialize(tag_name, args, tokens)
		super
		@title = args
	end

	def render(context)
<<-eos
<html>
<head>
<title>#{@title}</title>
<link type="text/css" href="/site.css" rel=stylesheet/>
</head>
<body bgcolor="white" topmargin="0" leftmargin="4" marginwidth="4" marginheight="0">
<div class="content">
#{super}
</div>
<hr/>
<div style='text-alignment: right; font-size: smaller;'>Copyright (C) kfs.org 2014</div>
</body>
</html>
eos
	end
end
Liquid::Template.register_tag('page', PageTag)

class TitleBoxTag < Liquid::Block
	def initialize(tag_name, args, tokens)
		super
	end

	def render(context)
<<-eos
<center>
<table class="titlebox" cellspacing="1">
<tr>
  <td width="480" height="160" valign="absmiddle">
   <img src="/logo-v2/480-160.jpg" width="480" height="160" align="absmiddle" alt="KingFisher Software"/>
  </td>
  <td>#{super}</td>
</tr>
</table>
<hr/>
</center>
eos
	end
end
Liquid::Template.register_tag('titlebox', TitleBoxTag)
