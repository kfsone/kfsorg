#################### Helper functions
#

# Fail with a diagnostic message and terminate
function die () {
	echo ERROR: $*
	exit 1
}

function debug () {
	if [ ! -z "${DEBUG}" ]; then echo "#DEBUG: $*"; fi
}

function test_for () {
	if [ ! $1 "$2" ]; then debug "$2 does not exist"; return 0; fi
	debug "$2 exists"
	return 1;
}

